//! \brief Parameter class selecting boundary conditions

class BCTypeParam
  : public Dune::PDELab::DirichletConstraintsParameters
{

  double time;

public:

  //! construct
  BCTypeParam()
    : time(0)
  {}

  //! Test whether boundary is Dirichlet-constrained
  template<typename I>
  bool isDirichlet(
                   const I & intersection,
                   const Dune::FieldVector<typename I::ctype, I::dimension-1> & coord
                   ) const
  {

    Dune::FieldVector<typename I::ctype, I::dimension>
      xg = intersection.geometry().global( coord );

    if (xg[0] > (1.0 - 1e-6))
      return false;
    else
      return true;
  }

  //! Test whether boundary is Neumann-constrained
  template<typename I>
  bool isNeumann(const I& intersection, const Dune::FieldVector<typename I::ctype, I::dimension-1>& xlocal) const
  {
    // For our problem, we have a Neumann boundary wherever there is no Dirichlet boundary
    return !isDirichlet(intersection,xlocal);
  }

  //! set time for subsequent evaluation
  void setTime (double t)
  {
    time = t;
  }

};
