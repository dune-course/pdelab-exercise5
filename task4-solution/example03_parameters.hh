//! \brief Parameter class selecting boundary conditions

template<typename RF>
class ProblemParameters
{

  RF time;

public:

  //! construct
  ProblemParameters()
    : time(0)
  {}

  //! Return Neumann flux
  template<typename I>
  RF j(const I& intersection, const Dune::FieldVector<typename I::ctype, I::dimension-1>& xlocal) const
  {
    return -0.5*(1.0 + cos(time));
  }

  //! Return reaction rate
  template<typename E, typename LocalCoord>
  RF a(const E& entity, const LocalCoord& xlocal) const
  {
    return 0;
  }

  //! Return right hand side
  template<typename E, typename LocalCoord>
  RF f(const E& entity, const LocalCoord& xlocal) const
  {
    return 0;
  }


  //! set time for subsequent evaluation
  void setTime (double t) {time = t;}
};
