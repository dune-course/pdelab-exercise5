#include"example02_operator.hh"
#include<dune/pdelab/localoperator/idefault.hh>

/** a local operator for solving the equation
 *
 *  \partial_t u - \Delta u + a*u = f   in \Omega
 *                              u = g   on \Gamma_D\subseteq\partial\Omega
 *             - \nabla u \cdot n = j   on \Gamma_N = \partial\Omega\setminus\Gamma_D
 *                              u = u_0 at t=t_0
 *
 * (spatial part!) with conforming finite elements on all types of grids in any dimension
 *
 * \tparam BCType parameter class for boundary condition type
 * \tparam P      parameter class for remaining model parameters
 */
template<typename BCType, typename P>
class Example03LocalOperator :
  public Example02LocalOperator<BCType,P>,
  public Dune::PDELab::InstationaryLocalOperatorDefaultMethods<double> // default methods
{

  BCType& bctype;
  P& p;

public:

  Example03LocalOperator (BCType& bctype_,
                          P& p_,
                          unsigned int intorder_=2)
    : Example02LocalOperator<BCType,P>(bctype_,p_,intorder_)
    , bctype(bctype_)
    , p(p_)
  {}

  void preStep (double time, double dt, int stages) {
    bctype.setTime(time + dt); // update time for boundary condition type parameter class
    Dune::PDELab::InstationaryLocalOperatorDefaultMethods<double>::preStep(time,dt,stages);
  }

  void setTime (double time) {
    p.setTime(time); // update time for problem parameter class
    Dune::PDELab::InstationaryLocalOperatorDefaultMethods<double>::setTime(time);
  }
};
