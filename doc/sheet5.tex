\documentclass[american,a4paper]{article}
\usepackage{babel}
\usepackage[utf8]{inputenc}
\usepackage{uebungen}
\usepackage{amsmath}
\usepackage{tikz}
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{listings}
\usepackage{wrapfig}
\usepackage[colorlinks=off]{hyperref}

\lstset{language=C++, basicstyle=\ttfamily,
  keywordstyle=\color{black}\bfseries, tabsize=4, stringstyle=\ttfamily,
  commentstyle=\it, extendedchars=true, escapeinside={/*@}{@*/}}

\title{\textbf{DUNE-PDELab exercises\\Friday, Feb~27$^\text{th}$~2015\\11:15-12:45}}
\dozent{Jorrit Fahlke $\quad$ Steffen Müthing}
\institute{IWR, University of Heidelberg}
\semester{}
\abgabetitle{}
%\abgabeinfo{ in der Übung}
\uebungslabel{Task}
\blattlabel{DUNE Workshop 2015 Exercise Sheet}

\newcommand{\vx}{\vec x}
\newcommand{\grad}{\vec \nabla}
\renewcommand{\div}{\vec\nabla\cdot}
\newcommand{\wind}{\vec \beta}
\newcommand{\Laplace}{\Delta}
\newcommand{\vn}{\vec n}
\newcommand{\mycomment}[1]{}

\begin{document}

\blatt[5]{Feb~27$^\text{th}$~2015}{}

\begin{uebung}{Getting to Know the Code}

  \lstset{language=bash}

  We'll start with example 3 from the lecture.  It solves the problem
  \begin{align}
    \partial_tu-\Laplace u+au&=f && \text{in }\Omega\times\Sigma,
                                           \Sigma=(t_0,t_0+T], \\
    u(\cdot,t)&=g(\cdot,t) && \text{on }\Gamma_D(t)\subseteq\partial\Omega, \\
    -\grad u(\cdot,t)\cdot\vn&=j(\cdot,t) &&
           \text{on }\Gamma_N(t)=\partial\Omega\setminus\Gamma_D(t), \\
    u(\cdot,t_0)&=u_0(\cdot) && \text{at } t=t_0
  \end{align}
  with the following choices applied:
  \begin{align}
    \label{ch1:first}
    a&=0 \\
    f&=0 \\
    \Gamma_D(t)&=\{\vx\in\partial\Omega|x_0 < 1\} \\
    g_\text{ext}(\vx,t)&=\begin{cases}
      \sin(t) & x_0=0\text{ and }0.25\leq x_1\leq0.5 \\
      0 & \text{else}
    \end{cases}\\
    g(\vx,t)&=g_\text{ext}(\vx,t)\qquad\forall\vx\in\Gamma_D(t) \\
    j(\vx,t)&=\begin{cases}
      +1 & x_1 < 0.5 \\
      -1 & \text{else}
    \end{cases} \\
    u_0(\vx)&=g_\text{ext}(\vx,t_0) \\
    \label{ch1:last}
    t_0&=0
  \end{align}

  All of the code for this exercise is in a module
  \lstinline!pdelab-exercise5! in your home directory.  You can
  recompile the tasks individually by changing into a task's directory
  \textbf{in your build directory} and typing make:
  % \footnote{You can recompile the whole exercise 5 module
  %   including the build system with the following command from your home
  %   directory:
  %   \begin{lstlisting}^^J
  %     dunecontrol --opts=parallel.opts --only=pdelab-exercise5 all^^J
  %   \end{lstlisting}
  % }
  \begin{lstlisting}
    dune001@cip54:~$ cd dune-course/pdelab-exercise5/release-build/task1
    dune001@cip54:~/dune-course/pdelab-exercise5/release-build/task1$ make
  \end{lstlisting}

  You can find the code for the first part in the subdirectory
  \lstinline!task1!.  The code consists of the following files:
  \begin{itemize}
  \item \lstinline!example03.cc! -- main program,
  \item \lstinline!example03_Q2.hh! -- driver to solve problem on a gridview,
    hard-codes $t_0=0$, $g(\vx,t)=g_\text{ext}(\vx,t)$ and
    $u_0(\vx)=g_\text{ext}(\vx,t_0)$
  \item \lstinline!example03_bctype.hh! -- boundary condition type parameter class,
    defined $\Gamma_D(t)$ and $\Gamma_N(t)$,
  \item \lstinline!example03_bcextension.hh! -- $g_\text{ext}(\vx,t)$
  \item \lstinline!example03_operator.hh! -- instationary spatial local
    operator $r(u,v;t)$
  \item \lstinline!example02_operator.hh! -- stationary local operator from
    example 5, used as a base for the instationary spatial local operator,
    $a$, $f$ and $j(\vx)$ are hard-coded here.
  \item \lstinline!example03_toperator.hh! -- instationary temporal local
    operator $m(u,v;t)$
  \end{itemize}
  Please read and understand the code if you have not done so already.
  Compile \lstinline!example03!, run it and examine the result with ParaView.
  If you run the program without arguments, it will print a short help
  message:
  \begin{lstlisting}
usage: ./example03 <level> <dt> <tend>
  \end{lstlisting}
  \lstinline!<level>! is the number of
  \lstinline[language=C++]!globalRefine()!s the program should do after
  creating a $1\times1$ grid.  \lstinline!<dt>! is the time step size, and
  \lstinline!<tend>! is the time $T$ at which the simulation should stop.

\end{uebung}

Once you feel ready you can proceed with the other tasks.  You can start with
any task you like, although it is suggested to do task 3 before task 4.

\begin{uebung}{Making Discretizations Easily Exchangeable}

  \begin{wrapfigure}[9]{l}{4.5cm}%
    \centering%
    \begin{tikzpicture}[scale=.25]
      \draw (0,0) rectangle (16,16);
      \shade[shading=radial] ( 3, 3) rectangle ( 7, 7);
      \shade[shading=radial] ( 9, 3) rectangle (13, 7);
      \shade[shading=radial] ( 9, 9) rectangle (13,13);
      \shade[shading=radial] ( 3, 9) rectangle ( 7,13);
      \shade[shading angle=0]   ( 5, 3) rectangle (11, 5);
      \shade[shading angle=90]  (11, 5) rectangle (13,11);
      \shade[shading angle=180] ( 5,11) rectangle (11,13);
      \shade[shading angle=270] ( 3, 5) rectangle ( 5,11);
      \filldraw[gray] (5,5) rectangle (11,11);
      \node at (8,8) {$u_0\!=\!1$};
      \node at (8,2) {$u_0=0$};
    \end{tikzpicture}%
    \caption{Initial conditions}
  \end{wrapfigure}
  Consider the initial and boundary conditions
  (\ref{ch1:first})--(\ref{ch1:last}) modified as follows:
  \begin{align}
    \label{ch2:first}
    \Gamma_D(t)&=\emptyset \\
    g_\text{ext}(\vx, t) &= \prod_{i=0}^{\text{\lstinline!dim!}-1}\min\{1,\max\{0,f(x_i)\}\} \\
        & \qquad\qquad f(\xi):=0.5-8(|\xi-0.5|-0.25) \nonumber \\
    \label{ch2:last}
    j(\vx,t)&=0
  \end{align}
  i.e.\ homogeneous Neumann boundary conditions everywhere, and for the
  initial conditions a block of constant 1 concentration in the middle,
  constant 0 concentration at the borders and some multi-linear slope
  in-between.

  On a $16\times16$ or finer grid the initial values can be represented
  exactly by Q1 and Q2 finite elements.  The exact solution will instantly
  become smooth and tend toward the mean over time.  A computed solution is
  only an approximation, and may show different behavior.  Most often it may
  take a long time for the solution to become smooth, and there are spikes
  oscillating from one time step to the next.

  In this exercise we want to examine how the solution behaves for different
  discretization schemes.  We will use two different spatial discretizations
  -- Q1 and Q2 -- and three temporal discretizations -- Implicit Euler,
  Crank-Nicholson and Fractional Step $\theta$.

  \paragraph{Step 1: Arbitrary Finite Elements}

  The directory \lstinline!pdelab-exercise5/task2! contains a copy of the
  directory \lstinline!pdelab-exercise5/task1! with some small modifications.
  As a first step, modify the driver function to support arbitrary finite
  elements.  The name of the driver routine has already been changed to
  \lstinline[language=C++]!example03_generic()! and its filename to
  \lstinline!example03_generic.hh!.  Please use the following function
  signature: \lstset{language=C++}
  \begin{lstlisting}
// GV:  GridView
// FEM: FiniteElementMap
template<class GV, class FEM>
void example03_generic(const GV& gv, const FEM& fem,
                       double dt, double tend, const char* fileprefix);
  \end{lstlisting}
  The main function can now use a call like
  \begin{lstlisting}
typedef GV::ctype Coord;
typedef double Real;
example03_generic(gv,
                  Dune::PDELab::QkLocalFiniteElementMap<GV,Coord,Real,2>(gv),
                  dt, tend, "example03_Q2");
  \end{lstlisting}
  to call the driver function with Q2 finite elements.

  Here is a checklist of the things to change:
  \begin{enumerate}
  \item Delete the definition of the finite element map from the body of
    \lstinline!example03_generic()!.
  \item The \lstinline!Real! type was previously hard-coded to double and
    later used as the range field type of the finite element map.  Now the
    function gets the finite element map as an argument with the range field
    type already fixed.  So the \lstinline!Real! type should be extracted from
    the finite element map:
    \begin{lstlisting}
typedef typename FEM::Traits::FiniteElementType::Traits::
  LocalBasisType::Traits::RangeFieldType Real;
    \end{lstlisting}
  \item The output filename prefix is currently hard-coded, change it to use
    the new \lstinline!fileprefix! function argument.  This allows you to have
    different output files for e.g.\ Q1 and Q2.
  \end{enumerate}
  Compile and run your program to see if it still works!

  \paragraph{Step 2: Arbitrary One Step Time Schemes}

  Now extend \lstinline!example03_generic()! to also take a parameter class for
  the one step scheme as an argument.  Please use the following
  signature:\footnote{Since the one step parameter classes all derive from a
    common abstract base class (for a given \lstinline!Real! type), and the
    \lstinline!Real! type is known from the \lstinline!FEM! class, one can in
    principle get away without the third template parameter.  This however
    makes the declaration of \lstinline!example03_generic()! quite ugly and is
    therefore left as an exercise to anyone interested...}
  \begin{lstlisting}
// GV:  GridView
// FEM: FiniteElementMap
// TSP: TimeSteppingParameters, defines time-scheme coefficients
template<class GV, class FEM, class TSP>
void example03_generic(const GV& gv, const FEM& fem, const TSP& method,
                       double dt, double tend, const char* fileprefix);
  \end{lstlisting}
  The main function can now use a call like
  \begin{lstlisting}
example03_generic(gv,
                  Dune::PDELab::QkLocalFiniteElementMap<GV,Coord,Real,2>(gv),
                  Dune::PDELab::ImplicitEulerParameter<Real>(),
                  dt, tend, "example03_Q2_IE");
  \end{lstlisting}
  to call the driver function with Q2 finite elements and the Implicit Euler
  time scheme.  Don't forget to delete the definition of the one step
  parameter from the body of \lstinline!example03_generic()!, then compile and
  run your program to see if it still works.

  \paragraph{Step 3: Proper Initial and Boundary Conditions}

  Please adapt the initial and boundary conditions to match
  (\ref{ch2:first})--(\ref{ch2:last}).  When implementing $g_\text{ext}$ you
  may be tempted to use the function \lstinline!abs()!.  This is wrong, use
  \lstinline!fabs()! or \lstinline!std::abs()!
  instead.\footnote{\lstinline!abs()! is a function from the C-library to
    compute absolute values for {\em integers}.  If you plug in a floating
    point value, it is truncated to an integer.  \lstinline!fabs()! is the
    C-library's function for doubles, and \lstinline!std::abs()! is the C++
    library's function template for any data type supporting the \lstinline!<!
    operator.}

  Compile and run your program.
  Remember that the grid needs $16\times16$ elements for the Q1/Q2 elements to
  resolve the initial condition exactly.  What happens to the interpolated
  initial condition if you use a coarser mesh?

  \paragraph{Step 4: All Your Schemes Are Belong To Us!}

  Adapt the \lstinline!main()! function to call the driver function for Q1 and
  Q2 spatial discretizations and for Implicit Euler, Crank-Nicholson and
  Fractional Step $\theta$ temporal discretizations (6 discretizations in
  total).

  Note that there is no special one step parameter class for
  Crank-Nicholson.  Crank-Nicholson is however the special case of the one
  step $\theta$ scheme with $\theta=0.5$.  So you can create a parameter
  object for Crank-Nicholson with
  \lstinline!Dune::PDELab::OneStepThetaParameter<Real>(0.5)!.  A parameter
  object for the Fractional Step $\theta$ scheme can be created with
  \lstinline!Dune::PDELab::FractionalStepParameter<Real>()!.

  \paragraph{Step 5: Compile with Optimizations}

  For the following study it saves time to recompile the program with
  optimizations enabled. Therefore be sure to remember that you are
  inside the \lstinline!release-build!.
  \begin{lstlisting}[language=bash]
    dune001@cip54:~$ cd ~/exercises/pdelab-exercise5/release-build/task2
    dune001@cip54:~/pdelab-exercise5/release-build/task2$ make
  \end{lstlisting}

  \paragraph{Step 6: Investigate Maximum Principle}

  \lstset{language=bash} With these preparations done, it is now time to
  actually check how the different discretizations perform.  Run your program
  to produce some output that you can examine in ParaView.  Use a $64\times64$
  grid (\lstinline!<level>!$\,=6$) and a matching
  \lstinline!<dt>!$\,=1/64=0.015625$.  Run the simulation until
  \lstinline!<tend>!$\,=0.0625$, i.e.\ for four time steps.  When examining
  the solution in ParaView, apply the ``Warp (Scalar)'' filter to get an image
  distorted into the third dimension according to the values of the solution.

  After one time step, the solution computed by Q1 finite elements with
  Implicit Euler time stepping should be completely smooth.  The same goes for
  Q2 with Implicit Euler.

  With both Crank-Nicholson and Fractional Step $\theta$, both with Q1 and Q2
  the solution should be quite non-smooth, i.e.\ there should be some edges
  visible.  The Fractional Step $\theta$ scheme should be smoother than
  Crank-Nicholson.

  Try to run the simulation with smaller time steps.  How small do you need to
  make the time steps to get smooth solutions with Fractional Step $\theta$?

  Can you get the Crank-Nicholson scheme to produce smooth solutions as well?

\end{uebung}

\begin{uebung}{Time Dependent $\Gamma_D$ and $\Gamma_N$}

  \lstset{language=C++} Imagine a block of some material being periodically
  dipped into a pool of warm water.  In the water, the material assumes
  the temperature of the water, while outside a cold wind constantly
  transports away the heat stored in the material.  We will approximate the
  dipping by a sine in $x_1$ direction, the warm water by a Dirichlet
  boundary condition of $1$ and the wind by a Neumann boundary condition of
  $-1$ in the following way:
  \begin{align}
    \Gamma_D(t)&=\{\vx\in\partial\Omega|x_1<\sin(t)/2+0.5\} \\
    g_\text{ext}(\vx,t)&=1 \\
    j(\vx,t)&=-1
  \end{align}
  Please implement the boundary conditions accordingly!

\end{uebung}

\begin{uebung}{Time Dependent Neumann Boundary Conditions}

  \lstset{language=C++}
  Imagine a barbecue.  Someone tried to light the coals with the help of
  diesel but all the matches were drowned in it.\footnote{Contrary to regular
    gasoline or spirit, you usually can't light diesel with a match.  Of
    course, the coals or the matches might just double as wigs, so while
    improbable it is still quite possible\ldots} So he decides to do this
  properly and fetches a heat gun.\footnote{There is nothing better to light
    coals than a heat gun.  You don't even have to light the coals with paper
    beforehand or anything.  Of course, you should concentrate the heat onto
    one point until it lights up instead of waving it around.}\footnote{I
    haven't tried whether a heat gun is able to ignite diesel, I guess it
    depends on the particular type of heat gun.  In any case I recommend a
    certain distance to the diesel-drenched coals at the point of
    ignition.\footnotemark While there probably won't be an explosion as such
    the heat of the initial flame can be surprisingly intense.  You should
    also have a few shovels of sand handy in case some burning diesel gets out
    of the grill, and gravity decides to show it that rather easily flammable
    stuff at the local minimum of elevation.}\footnotetext{This can achieved
    by taping the heat gun to a stick.  Once the coals are ignited, this
    technique has the additional advantage that sparks of glowing coal can't
    reach your skin as easily when a coal bursts due to the internal heat
    gradients.  Also, you can get the heat gun much closer to the coals
    without getting grilled yourself.\footnotemark}\footnotetext{Don't be
    alarmed when the plastic at the tip of the heat gun starts melting or even
    burning.  This is quite normal and that stuff is only dead weight anyway.}
  As he slowly waves it over the coals, let us concentrate at a single coal.
  For simplicity, let us assume it has the shape of a cube.  Normally it is
  nearly covered in diesel, which is at temperature $g$.  Over time, more and
  more heat is dumped into the diesel, resulting in a linear temperature
  increase $g\propto t$.  Each time the heat gun points at the piece of coal
  however, the coal is hit by an intense gust of super-heated air, which results in
  a heat flux $-j$ into the coal.  Simultaneously the diesel yields to the
  beam of air, releasing the coal nearly completely.

  This setup can be roughly modeled in the following way:
  \begin{align}
    \Gamma_D(t)&=\{\vx\in\partial\Omega|x_1<0.5-\cos(t)/2\} \\
    g_\text{ext}(\vx,t)&=t/10 \\
    j(\vx,t)&=-(0.5+\cos(t)/2)
  \end{align}

  Implementing the first two is easy, $g_\text{ext}(\vx,t)$ is time-dependent
  even in the example given in the lecture, and the time dependency in
  $\Gamma_D(t)$ was introduced in task 3.

  The time dependency in $j(\vx,t)$ is more complicated: $j(\vx,t)$ is
  implemented in \lstinline!Example02LocalOperator!, but that operator is
  stationary, it does not know anything about the time.  The trick is to
  separate evaluation of the Neumann boundary condition value into an external
  parameter class. In this case, the easiest place to do so is probably the parameter
  class that already gets passed to the local operator. This class already knows about
  the time (for determining the boundary condition type). Add a method
  \begin{lstlisting}
template<typename I>
RF j(const I& intersection,
     const Dune::FieldVector<typename I::ctype,I::dimension-1>& xlocal) const
\end{lstlisting}
  that returns the Neumann flux at the given position and adjust the local
  operator to call that method. To avoid hardcoding the return type of
  \lstinline!j()!, you should add a template parameter RF to the parameter class.

  There is one subtle problem with this approach: The time for the parameter
  function is set in the method \lstinline!preStep()! in
  \lstinline!Example03LocalOperator!, and is thus kept fixed during the complete
  time step. This is to avoid numerical problems: In principle, the type of the
  boundary condition may of course change at any time, but changing the
  boundary condition type in the middle of a time step requires some thought. On
  the other hand, adjusting the actual value of the Neumann boundary condition
  is much less problematic. To overcome this limitation, split the parameter class into two
  distinct classes, one for the boundary condition type and one for the Neumann
  boundary condition value, and pass both parameter classes to the local
  operator.\footnote{Adding a second parameter class for a single parameter
    might seem overkill here, but in a real program, you probably want to be
    able to adjust more parameters of your model, like the diffusion and
    the reaction coefficient or the right hand side, without having to poke
    around in the code of your local operator. This second parameter class
    would be the perfect place to place all those parameter evaluations.}
  Then you can update the time for the second parameter class in the method
  \lstinline!setTime()! in \lstinline!Example03LocalOperator!, which
  gets called every time before the operator is evaluated. \lstinline!Example03LocalOperator!
  already contains an empty template of that method that is currently commented
  out.


  Good parameters to run your program are
  \begin{lstlisting}
./example03 4 0.125 20
  \end{lstlisting}
  In the result the coal should quickly develop a temperature gradient, with
  the lower temperature end at the boundary to the diesel.  The temperature
  should be highest at the two corners of the domain which are not in contact
  with the diesel.  This is consistent with the expectation: when a heat gun
  is pointed at a coal, it will first start glowing at an edge.

  ParaView isn't very good at determining the correct color scale for instationary
  data sets. If the output just alternates between all blue and all red,
  try fixing the value range for the color scale to $[0,4]$ in ParaView.

\end{uebung}

\paragraph{Hints}
\begin{itemize}
\item Dune-PDELab homepage:\\
  \url{http://www.dune-project.org/pdelab/}
\item Dune-PDELab documentation online:\\
  \url{http://www.dune-project.org/doc-pdelab-trunk/doxygen/html/}
\end{itemize}

\end{document}

%%% Local Variables:
%%% mode: word-wrap
%%% TeX-master: t
%%% mode: flyspell
%%% ispell-local-dictionary: "american"
%%% End:
