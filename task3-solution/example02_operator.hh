#include<dune/geometry/quadraturerules.hh>
#include<dune/geometry/referenceelements.hh>

#include<dune/pdelab/localoperator/defaultimp.hh>
#include<dune/pdelab/localoperator/flags.hh>
#include<dune/pdelab/localoperator/pattern.hh>

/** a local operator for solving the equation
 *
 *   - \Delta u + a*u = f   in \Omega
 *                  u = g   on \Gamma_D\subseteq\partial\Omega
 *  -\nabla u \cdot n = j   on \Gamma_N = \partial\Omega\setminus\Gamma_D
 *
 * with conforming finite elements on all types of grids in any dimension
 *
 * \tparam BCType a function indicating the type of boundary condition
 */
template<class BCType>
class Example02LocalOperator :
  public Dune::PDELab::NumericalJacobianApplyVolume<Example02LocalOperator<BCType> >,
  public Dune::PDELab::NumericalJacobianVolume<Example02LocalOperator<BCType> >,
  public Dune::PDELab::NumericalJacobianApplyBoundary<Example02LocalOperator<BCType> >,
  public Dune::PDELab::NumericalJacobianBoundary<Example02LocalOperator<BCType> >,
  public Dune::PDELab::FullVolumePattern,
  public Dune::PDELab::LocalOperatorDefaultFlags
{
public:
  // pattern assembly flags
  enum { doPatternVolume = true };

  // residual assembly flags
  enum { doAlphaVolume = true };
  enum { doAlphaBoundary = true };                                // assemble boundary

  Example02LocalOperator(
                         const BCType& bctype_ // needs boundary cond.type
                         , unsigned int intorder_=2
                         )
    : bctype( bctype_ )
    , intorder( intorder_ )
  {}

  // volume integral depending on test and ansatz functions
  template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
  void alpha_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r) const
  {
    // extract some types
    typedef typename LFSU::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::DomainFieldType DF;
    typedef typename LFSU::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::RangeFieldType RF;
    typedef typename LFSU::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::JacobianType JacobianType;
    typedef typename LFSU::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::RangeType RangeType;
    typedef typename LFSU::Traits::SizeType size_type;

    // dimensions
    const int dim = EG::Geometry::dimension;
    const int dimw = EG::Geometry::dimensionworld;

    // select quadrature rule
    Dune::GeometryType gt = eg.geometry().type();
    const Dune::QuadratureRule<DF,dim>&
      rule = Dune::QuadratureRules<DF,dim>::rule(gt,intorder);

    // loop over quadrature points
    for (const auto& ip : rule)
      {
        // evaluate basis functions on reference element
        std::vector<RangeType> phi(lfsu.size());
        lfsu.finiteElement().localBasis().evaluateFunction(ip.position(),phi);

        // compute u at integration point
        RF u=0.0;
        for (size_type i=0; i<lfsu.size(); i++)
          u += x(lfsu,i)*phi[i];

        // evaluate gradient of basis functions on reference element
        std::vector<JacobianType> js(lfsu.size());
        lfsu.finiteElement().localBasis().evaluateJacobian(ip.position(),js);

        // transform gradients from reference element to real element
        const Dune::FieldMatrix<DF,dimw,dim>
          jac = eg.geometry().jacobianInverseTransposed(ip.position());
        std::vector<Dune::FieldVector<RF,dim> > gradphi(lfsu.size());
        for (size_type i=0; i<lfsu.size(); i++)
          jac.mv(js[i][0],gradphi[i]);

        // compute gradient of u
        Dune::FieldVector<RF,dim> gradu(0.0);
        for (size_type i=0; i<lfsu.size(); i++)
          gradu.axpy(x(lfsu,i),gradphi[i]);

        // evaluate parameters;
        // Dune::FieldVector<RF,dim>
        //   globalpos = eg.geometry().global(ip.position());
        RF f = 0;
        RF a = 0;

        // integrate grad u * grad phi_i + a*u*phi_i - f phi_i
        RF factor = ip.weight()*eg.geometry().integrationElement(ip.position());
        // BUGFIX: in the lecture, the two lines below incorrectly used "lfsu"
        // where "lfsv" should have been.  (The code did yield the correct
        // result anyway since lfsu == lfsv, but that isn't always the case.)
        for (size_type i=0; i<lfsv.size(); i++)
          r.accumulate(lfsv, i, (gradu*gradphi[i] + a*u*phi[i] - f*phi[i] ) * factor);
      }
  }

  // boundary integral
  template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
  void alpha_boundary (const IG& ig, const LFSU& lfsu_s, const X& x_s,
                       const LFSV& lfsv_s, R& r_s) const
  {
    // some types
    typedef typename LFSV::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::DomainFieldType DF;
    typedef typename LFSV::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::RangeFieldType RF;
    typedef typename LFSV::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::RangeType RangeType;
    typedef typename LFSV::Traits::SizeType size_type;

    // dimensions
    const int dim = IG::dimension;

    // select quadrature rule for face
    Dune::GeometryType gtface = ig.geometryInInside().type();
    const Dune::QuadratureRule<DF,dim-1>&
      rule = Dune::QuadratureRules<DF,dim-1>::rule(gtface,intorder);

    // loop over quadrature points and integrate normal flux
    for (const auto& ip : rule)
      {
        // skip rest if we are not on Neumann boundary
        if (!bctype.isNeumann(ig,ip.position()))
          continue;

        // position of quadrature point in local coordinates of element
        Dune::FieldVector<DF,dim> local = ig.geometryInInside().global(ip.position());

        // evaluate basis functions at integration point
        // BUGFIX: in the lecture, the line below incorrectly used "lfsv"
        // where "lfsu" should have been.  (The code did yield the correct
        // result anyway since lfsu == lfsv, but that isn't always the case.)
        std::vector<RangeType> phi(lfsu_s.size());
        lfsu_s.finiteElement().localBasis().evaluateFunction(local,phi);

        // evaluate u (e.g. flux may depend on u)
        RF u=0.0;
        for (size_type i=0; i<lfsu_s.size(); i++)
          u += x_s(lfsu_s,i)*phi[i];

        // evaluate flux boundary condition
        //Dune::FieldVector<RF,dim>
        //  globalpos = ig.geometry().global(ip.position());

        RF j = 1; // constant outflow

        // integrate j
        RF factor = ip.weight()*ig.geometry().integrationElement(ip.position());
        for (size_type i=0; i<lfsv_s.size(); i++)
          r_s.accumulate(lfsv_s,i,j*phi[i]*factor);
      }
  }

private:
  const BCType& bctype;
  unsigned int intorder;
};
