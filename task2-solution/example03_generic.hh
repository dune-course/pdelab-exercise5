// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=8 sw=2 sts=2:

// GV:  GridView
// TSP: TimeSteppingParameters, defines time-scheme coefficients
template<class GV, int k, class TSP>
void example03_generic(const GV& gv, const TSP& method,
                       double dt, double tend, const char* fileprefix)
{
  // <<<1>>> Choose domain and range field type
  typedef typename GV::Grid::ctype Coord;
  typedef double Real;

  Real time = 0.0;                                              // make a time variable

  // <<<2>>> Make grid function space
  typedef Dune::PDELab::QkLocalFiniteElementMap<GV,Coord,Real,k> FEM;
  FEM fem(gv);
  typedef Dune::PDELab::ConformingDirichletConstraints CON;
  typedef Dune::PDELab::istl::VectorBackend<> VBE;
  typedef Dune::PDELab::GridFunctionSpace<GV,FEM,CON,VBE> GFS;
  GFS gfs(gv,fem);
  gfs.name("solution");
  typedef BCTypeParam BCType;
  BCType b;
  b.setTime(time);                                              // b.c. depends on time now
  typedef typename GFS::template ConstraintsContainer<Real>::Type CC;
  CC cc;
  Dune::PDELab::constraints(b,gfs,cc);

  // <<<3>>> Make instationary grid operator space
  typedef Example03LocalOperator<BCType> LOP;
  LOP lop(b,2*k);                                                 // local operator r
  typedef Example03TimeLocalOperator TLOP;
  TLOP tlop(2*k);                                                 // local operator m
  typedef Dune::PDELab::istl::BCRSMatrixBackend<> MBE;
  // Structured 2D grid, Q1 finite elements => 9-point stencil / Q2 => 25
  MBE mbe(k == 1 ? 9 : 25);
  

  // <<<3a>>> Make (stationary) grid operator for 0-th time derivative
  typedef Dune::PDELab::GridOperator<
    GFS,GFS,
    LOP,
    MBE,
    Real,Real,Real,
    CC,CC
    > GO0;
  GO0 go0(gfs,cc,gfs,cc,lop,mbe);

  // <<<3b>>> Make (stationary) grid operator for first time derivative
  typedef Dune::PDELab::GridOperator<
    GFS,GFS,
    TLOP,
    MBE,
    Real,Real,Real,
    CC,CC
    > GO1;
  GO1 go1(gfs,cc,gfs,cc,tlop,mbe);


  // <<<3c>>> Plug grid operators together to form a OneStepGridOperator
  typedef Dune::PDELab::OneStepGridOperator<GO0,GO1> IGO;
  IGO igo(go0,go1);

  // How well did we estimate the number of entries per matrix row?
  // => print Jacobian pattern statistics
  //typename IGO::Traits::Jacobian jac(igo);
  //std::cout << jac.patternStatistics() << std::endl;

  // <<<4>>> Make FE function with initial value / Dirichlet b.c.
  typedef typename IGO::Traits::Domain U;
  U uold(gfs,0.0);                                              // solution at t^n
  typedef BCExtension<GV,Real> G;                               // defines boundary condition,
  G g(gv);                                                      // extension and initial cond.
  g.setTime(time);                                              // b.c. depends on time now
  Dune::PDELab::interpolate(g,gfs,uold);


  // <<<5>>> Select a linear solver backend
  typedef Dune::PDELab::ISTLBackend_SEQ_BCGS_SSOR LS;
  LS ls(5000,false);

  // <<<6>>> Solver for linear problem per stage
  typedef Dune::PDELab::StationaryLinearProblemSolver<IGO,LS,U> PDESOLVER;
  PDESOLVER pdesolver(igo,ls,1e-10);

  // <<<7>>> time-stepper
  Dune::PDELab::OneStepMethod<Real,IGO,PDESOLVER,U,U> osm(method,igo,pdesolver);
  osm.setVerbosityLevel(2);                                     // time stepping scheme

  // <<<8>>> graphics for initial guess
  Dune::SubsamplingVTKSequenceWriter<GV> vtkwriter(gv,3,fileprefix,"","");
  Dune::PDELab::addSolutionToVTKWriter(vtkwriter,gfs,uold);
  vtkwriter.write(time,Dune::VTK::appendedraw);

  // <<<9>>> time loop
  U unew(gfs,0.0);                                              // solution to be computed
  while (time<tend-1e-8) {
      // do time step
      b.setTime(time+dt);                                       // compute constraints
      cc.clear();                                               // for this time step
      Dune::PDELab::constraints(b,gfs,cc);
      osm.apply(time,dt,uold,g,unew);                           // do one time step

      uold = unew;                                              // advance time step
      time += dt;

      // graphics
      vtkwriter.write(time,Dune::VTK::appendedraw);
    }
}
