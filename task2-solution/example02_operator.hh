#include<dune/geometry/quadraturerules.hh>
#include<dune/geometry/referenceelements.hh>

#include<dune/pdelab/localoperator/defaultimp.hh>
#include<dune/pdelab/localoperator/flags.hh>
#include<dune/pdelab/localoperator/pattern.hh>

/** a local operator for solving the equation
 *
 *   - \Delta u + a*u = f   in \Omega
 *                  u = g   on \Gamma_D\subseteq\partial\Omega
 *  -\nabla u \cdot n = j   on \Gamma_N = \partial\Omega\setminus\Gamma_D
 *
 * with conforming finite elements on all types of grids in any dimension
 *
 * \tparam BCType a function indicating the type of boundary condition
 */
template<class BCType>
class Example02LocalOperator :
  public Dune::PDELab::NumericalJacobianApplyVolume<Example02LocalOperator<BCType> >,
  public Dune::PDELab::NumericalJacobianVolume<Example02LocalOperator<BCType> >,
  public Dune::PDELab::NumericalJacobianApplyBoundary<Example02LocalOperator<BCType> >,
  public Dune::PDELab::NumericalJacobianBoundary<Example02LocalOperator<BCType> >,
  public Dune::PDELab::FullVolumePattern,
  public Dune::PDELab::LocalOperatorDefaultFlags
{
public:
  // pattern assembly flags
  enum { doPatternVolume = true };

  // residual assembly flags
  enum { doAlphaVolume = true };

  Example02LocalOperator(
                         const BCType& bctype_ // needs boundary cond.type
                         , unsigned int intorder_=2
                         )
    : bctype( bctype_ )
    , intorder( intorder_ )
  {}

  // volume integral depending on test and ansatz functions
  template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
  void alpha_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r) const
  {
    // extract some types
    typedef typename LFSU::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::DomainFieldType DF;
    typedef typename LFSU::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::RangeFieldType RF;
    typedef typename LFSU::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::JacobianType JacobianType;
    typedef typename LFSU::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::RangeType RangeType;
    typedef typename LFSU::Traits::SizeType size_type;

    // dimensions
    const int dim = EG::Geometry::dimension;
    const int dimw = EG::Geometry::dimensionworld;

    // select quadrature rule
    Dune::GeometryType gt = eg.geometry().type();
    const Dune::QuadratureRule<DF,dim>&
      rule = Dune::QuadratureRules<DF,dim>::rule(gt,intorder);

    // loop over quadrature points
    for (const auto& ip : rule)
      {
        // evaluate basis functions on reference element
        std::vector<RangeType> phi(lfsu.size());
        lfsu.finiteElement().localBasis().evaluateFunction(ip.position(),phi);

        // compute u at integration point
        RF u=0.0;
        for (size_type i=0; i<lfsu.size(); i++)
          u += x(lfsu,i)*phi[i];

        // evaluate gradient of basis functions on reference element
        std::vector<JacobianType> js(lfsu.size());
        lfsu.finiteElement().localBasis().evaluateJacobian(ip.position(),js);

        // transform gradients from reference element to real element
        const Dune::FieldMatrix<DF,dimw,dim>
          jac = eg.geometry().jacobianInverseTransposed(ip.position());
        std::vector<Dune::FieldVector<RF,dim> > gradphi(lfsu.size());
        for (size_type i=0; i<lfsu.size(); i++)
          jac.mv(js[i][0],gradphi[i]);

        // compute gradient of u
        Dune::FieldVector<RF,dim> gradu(0.0);
        for (size_type i=0; i<lfsu.size(); i++)
          gradu.axpy(x(lfsu,i),gradphi[i]);

        // evaluate parameters;
        // Dune::FieldVector<RF,dim>
        //   globalpos = eg.geometry().global(ip.position());
        RF f = 0;
        RF a = 0;

        // integrate grad u * grad phi_i + a*u*phi_i - f phi_i
        RF factor = ip.weight()*eg.geometry().integrationElement(ip.position());
        // BUGFIX: in the lecture, the two lines below incorrectly used "lfsu"
        // where "lfsv" should have been.  (The code did yield the correct
        // result anyway since lfsu == lfsv, but that isn't always the case.)
        for (size_type i=0; i<lfsv.size(); i++)
          r.accumulate(lfsv, i, (gradu*gradphi[i] + a*u*phi[i] - f*phi[i] ) * factor);
      }
  }

private:
  const BCType& bctype;
  unsigned int intorder;
};
