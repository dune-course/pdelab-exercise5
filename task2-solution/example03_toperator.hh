#include<dune/geometry/referenceelements.hh>
#include<dune/geometry/quadraturerules.hh>

#include<dune/pdelab/localoperator/defaultimp.hh>
#include<dune/pdelab/localoperator/flags.hh>
#include<dune/pdelab/localoperator/idefault.hh>
#include<dune/pdelab/localoperator/pattern.hh>

/** a local operator for the mass operator (L_2 integral)
 *
 * \f{align*}{
 \int_\Omega uv dx
 * \f}
 */
class Example03TimeLocalOperator
  : public Dune::PDELab::NumericalJacobianApplyVolume<Example03TimeLocalOperator>,
    public Dune::PDELab::NumericalJacobianVolume<Example03TimeLocalOperator>,
    public Dune::PDELab::FullVolumePattern,
    public Dune::PDELab::LocalOperatorDefaultFlags,
    public Dune::PDELab::InstationaryLocalOperatorDefaultMethods<double>
{
public:
  // pattern assembly flags
  enum { doPatternVolume = true };

  // residual assembly flags
  enum { doAlphaVolume = true };

  Example03TimeLocalOperator (unsigned int intorder_=2)
    : intorder(intorder_), time(0.0)
  {}

  //! set time for subsequent evaluation
  void setTime (double t) {time = t;}

  // volume integral depending on test and ansatz functions
  template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
  void alpha_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r) const
  {
    // dimensions
    const int dim = EG::Geometry::dimension;

    // extract some types
    typedef typename LFSU::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::DomainFieldType DF;
    typedef typename LFSU::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::RangeFieldType RF;
    typedef typename LFSU::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::RangeType Range;
    typedef typename LFSU::Traits::SizeType size_type;

    typedef typename LFSU::Traits::SizeType size_type;

    // select quadrature rule
    Dune::GeometryType gt = eg.geometry().type();
    const Dune::QuadratureRule<DF,dim>& rule = Dune::QuadratureRules<DF,dim>::rule(gt,intorder);

    // loop over quadrature points
    for (const auto& ip : rule)
      {
        // evaluate basis functions
        std::vector<Range> phi(lfsu.size());
        lfsu.finiteElement().localBasis().evaluateFunction(ip.position(),phi);

        // evaluate u
        RF u=0.0;
        for (size_type i=0; i<lfsu.size(); ++i)
          u += x(lfsu,i)*phi[i];

        // u*phi_i
        RF factor = ip.weight() * eg.geometry().integrationElement(ip.position());
        for (size_type i=0; i<lfsv.size(); ++i)
          r.accumulate(lfsv,i,u*phi[i]*factor);
      }
  }
private:
  unsigned int intorder;
  double time;
};
