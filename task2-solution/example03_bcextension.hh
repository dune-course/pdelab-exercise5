/** \brief A function that defines Dirichlet boundary conditions AND its extension to the interior 
 */
template<typename GV, typename RF>
class BCExtension
  : public Dune::PDELab::GridFunctionBase<Dune::PDELab::
           GridFunctionTraits<GV,RF,1,Dune::FieldVector<RF,1> >, BCExtension<GV,RF> >
{
  const GV& gv;
  RF time;
public:
  typedef Dune::PDELab::GridFunctionTraits<GV,RF,1,Dune::FieldVector<RF,1> > Traits;

  //! construct from grid view
  BCExtension (const GV& gv_) : gv(gv_) {}

  //! evaluate extended function on element
  inline void evaluate (const typename Traits::ElementType& e, 
                        const typename Traits::DomainType& xlocal,
                        typename Traits::RangeType& y) const
  {  
    const unsigned dim = Traits::GridViewType::Grid::dimension;
    typedef typename Traits::GridViewType::Grid::ctype ctype;
    Dune::FieldVector<ctype,dim> x = e.geometry().global(xlocal);
    y = 1.0;
    for(unsigned i = 0; i < dim; ++i) {
      ctype d = std::abs(x[i] - 0.5);
      typename Traits::RangeFieldType myval = 0.5 - 8.0*(d-0.25);
      if(myval < 0.0) myval = 0.0;
      else if(myval > 1.0) myval = 1.0;
      y *= myval;
    }
  }
  
  //! get a reference to the grid view
  inline const GV& getGridView () {return gv;}

  //! set time for subsequent evaluation
  void setTime (double t) {time = t;}
};
